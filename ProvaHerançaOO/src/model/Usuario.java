package model;

public class Usuario {
	private String nome;
	private String senha;
	private String nickname;
	private String cpf;
	private int alerta;
	private boolean banimento;
	
	public Usuario(){
		alerta = 0;
		banimento = false;
	}
	public boolean getBanimento() {
		return banimento;
	}

	public void setBanimento(boolean banimento) {
		this.banimento = banimento;
	}

	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setAlerta(int alerta){
		this.alerta = alerta;
	}
	public int getAlerta(){
		return alerta;
	}
	

}
