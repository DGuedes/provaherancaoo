package model;

import java.util.ArrayList;


public class Moderador extends Usuario {
	private ArrayList<String> comentarios;
	
	public Moderador(ArrayList<String> comentarios){
		this.comentarios = comentarios;
	}
	
	public void PedirBanirUsuarioSet(Usuario nomeUsuario, boolean banimento){
		nomeUsuario.setBanimento(banimento);		
	}
	
	public void AlertarUsuario(Usuario nomeUsuario, int alerta){
		nomeUsuario.setAlerta(alerta);
	}
	public String DeletarComentario(String codigoComentario){
		comentarios.remove(codigoComentario);
		return "Comentario Deletado";
	}

}
