package model;

import java.util.ArrayList;

public class Reinvidicacao {
	
	private int Pontuacao;
	private String codigoReinvidicacao;
	private int ordem;
	private String autor;
	private ArrayList<String> comentarios = new ArrayList<String>();
	
	
	
	public int getPontuacao() {
		return Pontuacao;
	}
	public void setPontuacao(int pontuacao) {
		Pontuacao = pontuacao;
	}
	public String getCodigoReinvidicacao() {
		return codigoReinvidicacao;
	}
	public void setCodigoReinvidicacao(String codigoReinvidicacao) {
		this.codigoReinvidicacao = codigoReinvidicacao;
	}
	public void setOrdem(int ordem){
		this.ordem = ordem;
	}
	public int getOrdem(){
		return ordem;
	}
	public void AdicionarComentario(String codigosComentario){
		comentarios.add(codigosComentario);		
	}
	public void setAutor(String autor){
		this.autor = autor;	
	}
	public String getAutor(){
		return autor;
	}
}
