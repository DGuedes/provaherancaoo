package model;

import java.util.ArrayList;

public class Administrador extends Usuario {
	
	private ArrayList<Usuario> listaUsuarios;
	
	public Administrador(ArrayList<Usuario> listaUsuarios){
		this.listaUsuarios = listaUsuarios;
	}
	
	public String ConfirmarBanimento(Usuario umUsuario, boolean banimento){
		if(banimento==true){
			listaUsuarios.remove(umUsuario);
			return "Usuario removido";
		}
		else
			return "Usuario não banido!";
	}
}
