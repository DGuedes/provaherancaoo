package test;

import static org.junit.Assert.*;

import model.Reinvidicacao;

import org.junit.Before;
import org.junit.Test;

public class TesteModelReinvidicacao {

	private Reinvidicacao umaReinvidicacao;
	
	@Before
	public void SetUp(){
		umaReinvidicacao = new Reinvidicacao();
	}
	
	@Test
	public void testSetGetAutor() {
		umaReinvidicacao.setAutor("Teste Nome");
		assertEquals("Teste Nome", umaReinvidicacao.getAutor());
	}
	@Test
	public void testSetGetCodigoReinvidicacao(){
		umaReinvidicacao.setCodigoReinvidicacao("55");
		assertEquals("55", umaReinvidicacao.getCodigoReinvidicacao());
	}
	@Test
	public void testSetGetOrdem(){
		umaReinvidicacao.setOrdem(1);
		assertEquals(1, umaReinvidicacao.getOrdem(), 0.01);
	}
	@Test
	public void testSetGetPontuacao(){
		umaReinvidicacao.setPontuacao(5);
		assertEquals(5, umaReinvidicacao.getPontuacao(), 0.01);
	}
	

}
