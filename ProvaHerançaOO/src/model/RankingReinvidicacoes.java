package model;

import java.util.ArrayList;

public class RankingReinvidicacoes {
	
	private ArrayList<Reinvidicacao> listaReinvidicacoes;
	private int maior = 0;
	
	
	public void MostrarRanking(){
		for(Reinvidicacao umaReinvidicacao: listaReinvidicacoes){
			for(Reinvidicacao contador:listaReinvidicacoes){
				if(umaReinvidicacao.getPontuacao()>maior){
					maior = umaReinvidicacao.getPontuacao();
					umaReinvidicacao.setOrdem(umaReinvidicacao.getOrdem()+1);
				}
			}
		}
		System.out.println("Ranking:");
		for(Reinvidicacao umaReinvidicacao: listaReinvidicacoes){
			System.out.println(umaReinvidicacao.getOrdem() + "Reinvidicacao:" + umaReinvidicacao.getCodigoReinvidicacao() + "Pontuaçao:" + umaReinvidicacao.getPontuacao() );
		}
		
	}
}
