package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Moderador;
import model.Usuario;

import org.junit.Before;
import org.junit.Test;

public class TesteModelModerador {

	private Moderador umModerador;
	private ArrayList<String> comentarios;
	private Usuario umUsuario; 
	private boolean banimento;
	private String codigoComentario = "Testecodigo";
	@Before
	public void SetUp() throws Exception{
		umModerador = new Moderador(comentarios);
		umUsuario = new Usuario();
		banimento = true;
	}
	
	@Test
	public void testSetGetPedirBanirUsuario() {
		umModerador.PedirBanirUsuarioSet(umUsuario, banimento);
		assertEquals(true, umUsuario.getBanimento());
	}

}
