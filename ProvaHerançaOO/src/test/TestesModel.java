package test;

import static org.junit.Assert.*;

import model.Usuario;

import org.junit.Before;
import org.junit.Test;

public class TestesModel {
	
	private Usuario umaConta;
	
	@Before
	public void SetUp() throws Exception{
		umaConta = new Usuario();
	}
	
	//testando Usuario
	
	@Test
	public void testSetGetNome() {
		umaConta.setNome("Teste nome");
		assertEquals("Teste nome", umaConta.getNome());
	}
	@Test
	public void testSetGetSenha(){
		umaConta.setSenha("Teste senha");
		assertEquals("Teste senha", umaConta.getSenha());
	}
	@Test
	public void testSetGetNickname(){
		umaConta.setNickname("Teste usuario");
		assertEquals("Teste usuario", umaConta.getNickname());
	}
	@Test
	public void testSetGetCpf(){
		umaConta.setCpf("Teste cpf");
		assertEquals("Teste cpf", umaConta.getCpf());
	}
	@Test
	public void testSetGet(){
		umaConta.setAlerta(5);
		assertEquals(5, umaConta.getAlerta());
	}
	
	

}
