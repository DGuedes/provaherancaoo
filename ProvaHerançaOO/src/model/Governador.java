package model;

import java.util.ArrayList;

public class Governador extends Usuario {
	private ArrayList<String> listaReinvidicacoes;
	
	public Governador(ArrayList<String> listaReinvidicacoes){		
		this.listaReinvidicacoes = listaReinvidicacoes;		
	}
	
	
	public String AceitarReinvidicacao(String codigoReinvidicacao, boolean reinvidicacao){
		if(reinvidicacao == true){
			listaReinvidicacoes.add(codigoReinvidicacao);
			return "Reinvidicacao aceita!";
		}
		else
			return "Reinvidicacao recusada!";
	}
}
