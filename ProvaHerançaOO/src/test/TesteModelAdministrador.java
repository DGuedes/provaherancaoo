package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Administrador;
import model.Usuario;

import org.junit.Before;
import org.junit.Test;

public class TesteModelAdministrador {

	private Administrador umAdministrador;
	private ArrayList<Usuario> listaUsuarios;
	private Usuario umUsuario;
	private boolean banimento;
	@Before
	public void SetUp(){
		listaUsuarios = new ArrayList<Usuario>();
		umAdministrador = new Administrador(listaUsuarios);
		umUsuario = new Usuario();
		banimento = true;
	}
	
	@Test
	public void testConfirmarBanimento() {
		umUsuario.setBanimento(true);
		assertEquals(true, umUsuario.getBanimento());
		String mensagem = umAdministrador.ConfirmarBanimento(umUsuario, banimento);
		assertEquals("Usuario removido", mensagem);
	}

}
